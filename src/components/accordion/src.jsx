import React, { useState } from 'react';
import './styles.css';

export const Accordion = ({ title, number, content }) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleAccordion = () => {
        setIsOpen(!isOpen);
    };

    return (
        <div className="accordion-item">
            <section className="accordion-header" onClick={toggleAccordion}>
                <span className="accordion-number" style={isOpen? {color: "#087f5b"} : {color: "#868e96"}}>{number}</span>
                <p className="accordion-title">{title}</p>
                
                <span className='accordion-icon'>{
                    isOpen ?  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m4.5 15.75 7.5-7.5 7.5 7.5" />
                              </svg>
                            : <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m19.5 8.25-7.5 7.5-7.5-7.5" />
                              </svg>}
                </span>
            </section>
            { isOpen ? 
                <section className="accordion-content">
                    <p className='accordion-text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Metus aliquam eleifend mi in. Elit eget gravida cum sociis natoque penatibus et. Turpis massa tincidunt dui ut ornare lectus sit amet. Volutpat commodo sed egestas egestas fringilla phasellus faucibus. Tincidunt praesent semper feugiat nibh.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. </li>
                        <li>Laudantium nam laboriosam pariatur ullam libero dolores.</li>
                        <li>Quisquam, voluptatum. Quisquam, voluptatum.</li>
                    </ul>
                </section>
            : null }
        </div>
    );
};
