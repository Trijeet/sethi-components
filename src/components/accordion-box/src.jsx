import React, { useState } from 'react';
import './styles.css';
import { Accordion } from '../accordion/src';

export const AccordionBox = ({}) => {
    let content = "sample content";

    return (
        <div className="accordion-box">
            <Accordion number={"01"} title={"Where are these chairs assembled?"} content={content}/>
            <Accordion number={"02"} title={"How long do I have to return my chair?"} content={content}/>
            <Accordion number={"03"} title={"Do you ship to countries outside the EU?"} content={content}/>
        </div>
    );
};
