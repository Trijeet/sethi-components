import logo from './logo.svg';
import './App.css';
import { AccordionBox } from './components/accordion-box/src';

function App() {

  return (
    <div className="App">
      <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&display=swap"
        rel="stylesheet"
      />
      <AccordionBox />
    </div>
  );
}

export default App;
